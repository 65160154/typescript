interface Rectangle {
    width: number;
    height: number;

}

interface ColoresRectangle extends Rectangle {
    color: string;
}

const rectangle: Rectangle = {
    width: 20,
    height: 10
}
console.log(rectangle);

const coloresRectangle: ColoresRectangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(coloresRectangle);